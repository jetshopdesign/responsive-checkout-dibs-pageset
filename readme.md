# Responsive pageset for DIBS

HTML & CSS for DIBS's mananager. Two pages, full set and a minimal version.

**We recommend using the minimal version for all customer!**

## Instructions to support

* Open the minimal pageset for the language needed.

* Login to DIBS's manager and copy/paste content into corresponding pageset.

* Always grab code from this repo, do not save the pageset to external location!

## Instructions to update CSS

* In terminal cd to subfolder SCSS/. Compile the SCSS using ``grunt``

* Copy content from style.css and paste into/replace <style> tag in head for each pageset.

* Login to DIBS's manager and update content for corresponding pageset.

