module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        sass: {
            options: {
                includePaths: ['scss'],
                noCache: true
            },
            dev: {
                options: {
                    debugInfo: true,
                    trace: true,
                    lineNumbers: true,
                    outputStyle: 'nested',
                    sourceMap: 'style.css.map'
                },
                files: {
                    'style.css': 'dibs-responsive-checkout.scss'
                }
            },
            default: {
                options: {
                    outputStyle: 'nested'
                },
                files: {
                    '../style.css': 'dibs-responsive-checkout.scss'
                }
            }
        },

        watch: {
            sass: {
                files: '*.scss',
                tasks: ['sass:default']
            }
        }
    });

    // Load all libs
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');


    grunt.registerTask('default', ['sass:default']);
    grunt.registerTask('dev', ['sass:dev','watch']);
};